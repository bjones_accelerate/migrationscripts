
##################
#Enable debugging
#0 = Disabled
#1 = Enabled
#2 = Recursive logging, no delete
##################
$debug = 1
IF($debug -lt 2)
{
remove-item C:\ContractWork\Appno\debug.log
}
ELSE
{
echo Log not deleted, Recursive logging enabled
}



cls
#Pickup Creds from secure file
$user = (Get-Content C:\ContractWork\Appno\security_file.secure)[1]
$pwdencrypt = (Get-Content C:\ContractWork\Appno\security_file.secure)[3]
$url = (Get-Content C:\ContractWork\Appno\security_file.secure)[5]
$Public = (Get-Content C:\ContractWork\Appno\security_file.secure)[7]
$Private = (Get-Content C:\ContractWork\Appno\security_file.secure)[9]
$JIRAURL = (Get-Content C:\ContractWork\Appno\security_file.secure)[11]
$JIRAUser = (Get-Content C:\ContractWork\Appno\security_file.secure)[13]
$JIRAPasswd = (Get-Content C:\ContractWork\Appno\security_file.secure)[15]

function ConvertTo-Base64($string) {
    $bytes = [System.Text.Encoding]::UTF8.GetBytes($string);
    $encoded = [System.Convert]::ToBase64String($bytes);
    return $encoded;
    }

$Token1 = 0

$ImpersonationMember = 'acceleratetemp'

$CURL = "C:\Program Files\curl\bin\curl.exe"

#Encrypt password to base64string
#$encodedAuth = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(('passwordhere')));

#Comment out if password is plain txt
#$encodedAuthDeCrypt =  [System.Text.Encoding]::ASCII.GetString([System.Convert]::FromBase64String($pwdencrypt))

#Uncomment if password is plain txt
$encodedAuthDeCrypt = $pwdencrypt


#API pre-authentication
$CWServerRoot = $url
$CWInfocompany = 'appno'
$CWInfouser = $user
$CWInfopassword = $encodedAuthDeCrypt

cls

    [string]$BaseUri     = "$CWServerRoot" + "v4_6_release/apis/3.0/system/members/$ImpersonationMember/tokens"
    [string]$Accept      = "application/vnd.connectwise.com+json; version=v2017_2"
    [string]$ContentType = "application/json"
    #[string]$Authstring  = $CWInfocompany + '+' + $CWInfouser + ':' + $CWInfopassword
    [string]$Authstring  = $CWInfocompany + '+' + $Public + ':' + $Private

    #Convert the user and pass (aka public and private key) to base64 encoding
    $encodedAuth = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(($Authstring)));

    #Create Header
    $header = [Hashtable] @{
        Authorization = ("Basic {0}" -f $encodedAuth)
        Accept        = $Accept
        Type          = "application/json"
        #'x-cw-usertype' = "integrator"
        'x-cw-usertype' = "member" 
    };

    $body   = "{`"memberIdentifier`":`"$ImpersonationMember`"}"

    #execute the the request
    $response = Invoke-RestMethod -Uri $Baseuri -Method Post -Headers $header -Body $body -ContentType $contentType;

    $CWcredentials = $response

IF($debug -lt 2)
{
write-host "Debug enabled, pausing"
write-host "So far so good is no errors were thrown, shall we proceed?"
pause
}
ELSE
{

}




#API Access post authentication    
    [string]$Accept      = "application/vnd.connectwise.com+json; version=v2018_4"
    [string]$ContentType = "application/json"
    [string]$Authstring = "$CWInfocompany+" + $($CWcredentials.publickey) + ':' + $($CWCredentials.privatekey)
    $encodedAuth = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(($Authstring)));
#Company URL
    [string]$CompanyURI     = "$CWServerRoot" + "v4_6_Release/apis/3.0/company/companies/count"


#Body is ONLY needed to POST or PUT, this is NOT needed for GET calls
$Body= @"
{

}
"@
$Body

$Headers=@{
"Authorization"="Basic $encodedAuth"
}



    #CW Ticket Count Call
    $CWCompanyCount = $JSONResponse = Invoke-RestMethod -URI $CompanyURI -Headers $Headers -ContentType $ContentType -Method GET
    $Clients = $CWCompanyCount.Count

IF($debug -lt 2)
{
write-host "Debug enabled, pausing"
write-host "You are about to start the process to pull $Clients comapnies"
pause
}
ELSE
{

}

#Collect ALL Clients from Connectwise and store them within a variable array all within memory.
    $ClientArray = @()
    $JSONClients = 0
    [string]$IncTicUri = "$CWServerRoot" + "v4_6_Release/apis/3.0/company/companies?pagesize=1000"
    $JSONClients = Invoke-RestMethod -URI $IncTicUri -Headers $Headers -ContentType $ContentType -Method GET
    $ClientArray += $JSONClients
    #$TicketArray.id
    $ClientArray.count

#Collect ALL contacts 
<#
    $ContactArray = @()
    [string]$ContactURL = $CWServerRoot + "v4_6_release/apis/3.0/company/contacts?pagesize=1000"
    $JSONContacts = Invoke-RestMethod -URI $ContactURL -Headers $Headers -ContentType $ContentType -Method GET
    $ContactArray += $JSONContacts
    $ContactArray.count
#>

    $ObjectCount = $ClientArray | measure
    $C1 = $ObjectCount.count

###################################################
###################################################
###################################################
###################################################
#Gather all of the present JSD Orgs
###################################################
###################################################
###################################################
###################################################
IF($debug -lt 2)
{
write-host "Debug enabled, pausing"
write-host "You are about to start Debug testing"
write-host "Gathering all of the Orgs within JSD"
}

###################################################
    
    $b64 = ConvertTo-Base64($username + ":" + $password);
    $auth = "Basic " + $b64;
$NR = 0
$OC = 0
$Orgs = @()
$OrgID = @()
$OrgName = @()


DO{

    [String] $target = $JIRAURL;
    [String] $username = $JIRAUser
    [String] $password = $JIRAPasswd
    [String] $start = "start="+$NR
    [String] $limit = "limit=50"
    $requestUri = $target+"/rest/servicedeskapi/organization?$start&$limit"

        try {
            $headers = @{
                "Authorization" = $auth
                "Content-Type"="application/json"
                "X-ExperimentalApi"="opt-in"
            }
            $OrgInfo = Invoke-RestMethod -Uri $requestUri -Method GET -Headers $headers -UseBasicParsing

            }
        catch {
            Write-Warning "Remote Server Response: $($_.Exception.Message)"
            Write-Output "Status Code: $($_.Exception.Response.StatusCode)"
        }
        $OrgCount = $OrgInfo.values | Measure-Object

    IF (($OrgCount.Count -eq 0))
        {
        $MORE = "True"
        write-host "No orgs present within JSD"
        }
     ELSE
        {
            $Lines = $OrgInfo.Values.name  | Measure -Line
            $OrgID += $OrgInfo.Values.id
            $OrgName += $OrgInfo.Values.Name
            $LC = 0
            DO{
            $ID = $OrgInfo.Values.id[$LC]
            $NAME = $OrgInfo.Values.Name[$LC]
            $Orgs += $NAME +" @ " +$ID
            $LC++
            }
            UNTIL($LC -eq $Lines.Lines)

            $MORE = $OrgInfo.isLastPage
            $NR+=50
        }
    }
    UNTIL ($MORE -eq "True")
    $OrgName | Measure -Line

    

###################################################
###################################################
###################################################
###################################################
#This process below is what creates the arrays and starts creating tickets in Jira
###################################################
###################################################
###################################################
###################################################
        #$JSDID = 1
        #$ClientArray[25]
        #$ClientArray.type.name[50]

    $CTO = 0
    DO{

         #Add Org to Service Desk
        
        $ClientName = $ClientArray.name[$CTO]
        $ClientID = $ClientArray.identifier[$CTO]

        #Client org grouping, only a few will be needed:
           IF($ClientID -eq "BLUE") 
            {$ClientName = "Bluewolf"}
           IF($ClientID -eq "Bluewolf") 
            {$ClientName = "Bluewolf"}
           IF($ClientID -eq "bluewolf.com") 
            {$ClientName = "Bluewolf"}
           IF($ClientID -eq "ccbcu.com") 
            {$ClientName = "Coca-Cola United Bottling"} 
           IF($ClientID -eq "CCBU") 
            {$ClientName = "Coca-Cola United Bottling"}
           IF($ClientID -eq "CCBULL") 
            {$ClientName = "Coca-Cola United Bottling"}
           IF($ClientID -eq "POOL") 
            {$ClientName = "Poolcorp"}
           IF($ClientID -eq "Pool Corp") 
            {$ClientName = "Poolcorp"}
           IF($ClientID -eq "1800gotjunk.com") 
            {$ClientName = "1-800-Got-Junk"}
           IF($ClientID -eq "GJUNK") 
            {$ClientName = "1-800-Got-Junk"}
           IF($ClientID -eq "o2ebrands.com") 
            {$ClientName = "1-800-Got-Junk"}
           IF($ClientID -eq "NSMM") 
            {$ClientName = "National September 11 Memorial & Museum"}
           IF($ClientID -eq "911memorial.org") 
            {$ClientName = "National September 11 Memorial & Museum"}
           IF($ClientID -eq "SEPT11MM.onmicrosoft.com") 
            {$ClientName = "National September 11 Memorial & Museum"}
           IF($ClientID -eq "AAAC") 
            {$ClientName = "AAA Carolinas"}
           IF($ClientID -eq "ABSE") 
            {$ClientName = "Absec Malaysia Sdn Bhd"} 
           IF($ClientID -eq "ACTV") 
            {$ClientName = "Activar"}
           IF($ClientID -eq "ADDI") 
            {$ClientName = "Addic Store"}
           IF($ClientID -eq "ADMG") 
            {$ClientName = "Admiral Group"} 
           IF($ClientID -eq "AGRO") 
            {$ClientName = "Agropur"}
           IF($ClientID -eq "ALCA") 
            {$ClientName = "Alcatel"}
           IF($ClientID -eq "Appnovation Technologies Ltd") 
            {$ClientName = "Appnovation Technologies"}
           IF($ClientID -eq "appnovation.co.uk") 
            {$ClientName = "Appnovation Technologies"}
           IF($ClientID -eq "appnovation.com") 
            {$ClientName = "Appnovation Technologies"}
           IF($ClientID -eq "APTS") 
            {$ClientName = "America's Public Television Stations"}
           IF($ClientID -eq "apts.org") 
            {$ClientName = "America's Public Television Stations"}
           IF($ClientID -eq "ARTO") 
            {$ClientName = "Artona Group"}
           IF($ClientID -eq "artona.com") 
            {$ClientName = "Artona Group"}
           IF($ClientID -eq "ATNT") 
            {$ClientName = "AT&T"}
           IF($ClientID -eq "att.com") 
            {$ClientName = "AT&T"}
           IF($ClientID -eq "att.net") 
            {$ClientName = "AT&T"}
           IF($ClientID -eq "ATTE") 
            {$ClientName = "Attentia"}
           IF($ClientID -eq "AVAL") 
            {$ClientName = "Avalere Health"}
           IF($ClientID -eq "avalere.com") 
            {$ClientName = "Avalere Health"}
           IF($ClientID -eq "axa-asiassc.com") 
            {$ClientName = "AXA Asia"}
           IF($ClientID -eq "axa.com.hk") 
            {$ClientName = "AXA Asia"}
           IF($ClientID -eq "AXAA") 
            {$ClientName = "AXA Asia"}
           IF($ClientID -eq "AZGA") 
            {$ClientName = "AZG"}
           IF($ClientID -eq "BCAA") 
            {$ClientName = "BCAA"}
           IF($ClientID -eq "bcaa.com") 
            {$ClientName = "BCAA"}
           IF($ClientID -eq "BCBS") 
            {$ClientName = "Blue Cross Blue Shield of North Carolina"}
           IF($ClientID -eq "bcbsnc.com") 
            {$ClientName = "Blue Cross Blue Shield of North Carolina"}
           IF($ClientID -eq "BCLQ") 
            {$ClientName = "BC Liquor Distribution Branch"}
           IF($ClientID -eq "BEGE") 
            {$ClientName = "Beeld en geluid"}
           IF($ClientID -eq "berkeley.edu") 
            {$ClientName = "UC Berkeley"}
           IF($ClientID -eq "BMIR") 
            {$ClientName = "BMI Research"}
           IF($ClientID -eq "bmiresearch.com") 
            {$ClientName = "BMI Research"}
           IF($ClientID -eq "BOEA") 
            {$ClientName = "Bank of East-Asia"}
           IF($ClientID -eq "BOTA") 
            {$ClientName = "Botanique"}
           IF($ClientID -eq "BUSA") 
            {$ClientName = "BrandUSA"}
           IF($ClientID -eq "CANDI") 
            {$ClientName = "Camden and Islington NHS Foundation Trust"}
           IF($ClientID -eq "CARI") 
            {$ClientName = "Caritas/Caritas Vlaanderen"}
           IF($ClientID -eq "Catchall") 
            {$ClientName = "Catchall"}
           IF($ClientID -eq "CBCS") 
            {$ClientName = "CBC Steel"}
           IF($ClientID -eq "cbcsteelbuildings.com") 
            {$ClientName = "CBC Steel"}
           IF($ClientID -eq "CCBF") 
            {$ClientName = "Coca-Cola Beverages Florida"}
           IF($ClientID -eq "cocacolaflorida.com") 
            {$ClientName = "Coca-Cola Beverages Florida"}
           IF($ClientID -eq "CCBUCI") 
            {$ClientName = "Coca-Cola United Bottling (CodeIgniter)"}
           IF($ClientID -eq "CCMO") 
            {$ClientName = "Circuit Court of Missouri"}
           IF($ClientID -eq "CCOM") 
            {$ClientName = "Charter Communications"}
           IF($ClientID -eq "CENT") 
            {$ClientName = "Centrica"}
           IF($ClientID -eq "CITY") 
            {$ClientName = "CityYear.org"}
           IF($ClientID -eq "cityyear.org") 
            {$ClientName = "CityYear.org"}
           IF($ClientID -eq "CITYU") 
            {$ClientName = "City University Hong Kong"}
           IF($ClientID -eq "cloudsolutions.co.uk") 
            {$ClientName = "Cloud Technology Solutions"}
           IF($ClientID -eq "CLTE") 
            {$ClientName = "Cloud Technology Solutions"}
           IF($ClientID -eq "COKE") 
            {$ClientName = "Coca-Cola"}
           IF($ClientID -eq "arcacontal.com") 
            {$ClientName = "Coca-Cola"}
           IF($ClientID -eq "COLU") 
            {$ClientName = "Columbia University"}
           IF($ClientID -eq "columbia.edu") 
            {$ClientName = "Columbia University"}
           IF($ClientID -eq "ConnectWise") 
            {$ClientName = "ConnectWise"}
           IF($ClientID -eq "ConnectWise.com") 
            {$ClientName = "ConnectWise"}
           IF($ClientID -eq "CRFE") 
            {$ClientName = "Carrefour"}
           IF($ClientID -eq "CULT") 
            {$ClientName = "CultuurNet Vlaanderen vzw"}
           IF($ClientID -eq "CURA") 
            {$ClientName = "Curalia"}
           IF($ClientID -eq "DLRX") 
            {$ClientName = "Dans La Rue"}
           IF($ClientID -eq "danslarue.org") 
            {$ClientName = "Dans La Rue"}
           IF($ClientID -eq "dh.com") 
            {$ClientName = "D+H"}
           IF($ClientID -eq "DHFT") 
            {$ClientName = "D+H"}
           IF($ClientID -eq "DISP") 
            {$ClientName = "Dispatch Printing Company"}
           IF($ClientID -eq "DJOB") 
            {$ClientName = "Djobby"}
           IF($ClientID -eq "DJOG") 
            {$ClientName = "DJO Global"}
           IF($ClientID -eq "djoglobal.com") 
            {$ClientName = "DJO Global"}
           IF($ClientID -eq "DRUG") 
            {$ClientName = "Druglijn/VAD vzw"}
           IF($ClientID -eq "DRUP") 
            {$ClientName = "Drupal Association"}
           IF($ClientID -eq "ECON") 
            {$ClientName = "The Economist Group"}
           IF($ClientID -eq "economist.com") 
            {$ClientName = "The Economist Group"}
           IF($ClientID -eq "ECOS") 
            {$ClientName = "ECOstyle"}
           IF($ClientID -eq "EEGP") 
            {$ClientName = "Events East Group"}
           IF($ClientID -eq "ENFO") 
            {$ClientName = "Enfocus"}
           IF($ClientID -eq "ERIC") 
            {$ClientName = "Erickson Coaching International"}
           IF($ClientID -eq "excelii.com") 
            {$ClientName = "Excel Industries"}
           IF($ClientID -eq "EXI") 
            {$ClientName = "Excel Industries"}
           IF($ClientID -eq "exi-ssh.c.oscaddie-prod-1259.internal") 
            {$ClientName = "Excel Industries"}
           IF($ClientID -eq "EYER") 
            {$ClientName = "Eye Recommend"}
           IF($ClientID -eq "eyerecommend.ca") 
            {$ClientName = "Eye Recommend"}
           IF($ClientID -eq "FLEX") 
            {$ClientName = "FlexCare Medical Staffing"}
           IF($ClientID -eq "flexcarestaff.com") 
            {$ClientName = "FlexCare Medical Staffing"}
           IF($ClientID -eq "SSFT") 
            {$ClientName = "Ski Safari and Freedom Treks"}
           IF($ClientID -eq "freedomtreks.co.uk") 
            {$ClientName = "Ski Safari and Freedom Treks"}
           IF($ClientID -eq "skisafari.com") 
            {$ClientName = "Ski Safari and Freedom Treks"}
           IF($ClientID -eq "GDWR") 
            {$ClientName = "Guidewire Software"}
           IF($ClientID -eq "GENO") 
            {$ClientName = "Genomics England"}
           IF($ClientID -eq "genomicsengland.co.uk") 
            {$ClientName = "Genomics England"}
           IF($ClientID -eq "GIMV") 
            {$ClientName = "Gimv"}
           IF($ClientID -eq "GLCD") 
            {$ClientName = "Global Credit Data"}
           IF($ClientID -eq "GMAC") 
            {$ClientName = "Gemeente Maasmechelen-administratief centrum"}
           IF($ClientID -eq "GOLD") 
            {$ClientName = "Goldcorp"}
           IF($ClientID -eq "guidewire.com") 
            {$ClientName = "Guidewire Software"}
           IF($ClientID -eq "HOSP") 
            {$ClientName = "Hospital Authority"}
           IF($ClientID -eq "ha.org.hk") 
            {$ClientName = "Hospital Authority"}
           IF($ClientID -eq "HAMM") 
            {$ClientName = "Hammersmith and Fulham Council"}
           IF($ClientID -eq "HASS") 
            {$ClientName = "Helics - Hasselt"}
           IF($ClientID -eq "HCCU") 
            {$ClientName = "Heartland Coca-Cola Bottling Company"}
           IF($ClientID -eq "HERI") 
            {$ClientName = "Herita"}
           IF($ClientID -eq "HKTD") 
            {$ClientName = "Hong Kong Trade Development Council"}
           IF($ClientID -eq "hktdc.org") 
            {$ClientName = "Hong Kong Trade Development Council"}
           IF($ClientID -eq "hpe.com") 
            {$ClientName = "Hewlett Packard Enterprise"}
           IF($ClientID -eq "HPEN") 
            {$ClientName = "Hewlett Packard Enterprise"}
           IF($ClientID -eq "HUDY") 
            {$ClientName = "Hudson Yards"}
           IF($ClientID -eq "IAC") 
            {$ClientName = "IAC"}
           IF($ClientID -eq "ICON") 
            {$ClientName = "iconectiv"}
           IF($ClientID -eq "iconectiv.com") 
            {$ClientName = "iconectiv"}
           IF($ClientID -eq "IENE") 
            {$ClientName = "IE-Net v.z.w."}
           IF($ClientID -eq "inflightcorp.com") 
            {$ClientName = "Inflight"}
           IF($ClientID -eq "inflightintegration.com") 
            {$ClientName = "Inflight"}
           IF($ClientID -eq "INIT") 
            {$ClientName = "InitWeather"}
           IF($ClientID -eq "ITRS") 
            {$ClientName = "ITRS Group"}
           IF($ClientID -eq "itrsgroup.com") 
            {$ClientName = "ITRS Group"}
           IF($ClientID -eq "JANR") 
            {$ClientName = "Janrain"}
           IF($ClientID -eq "JCCA") 
            {$ClientName = "Judicial Council of California"}
           IF($ClientID -eq "jdpa.com") 
            {$ClientName = "J.D Power & Associates"}
           IF($ClientID -eq "JPOW") 
            {$ClientName = "J.D Power & Associates"}
           IF($ClientID -eq "KPNA") 
            {$ClientName = "KPN"}
           IF($ClientID -eq "LCSC") 
            {$ClientName = "Leicestershire City Council"}
           IF($ClientID -eq "leics.gov.uk") 
            {$ClientName = "Leicestershire City Council"}
           IF($ClientID -eq "LEUV") 
            {$ClientName = "Helics - Leuven"}
           IF($ClientID -eq "LLNL") 
            {$ClientName = "Lawrence Livermore National Laboratory"}
           IF($ClientID -eq "LOCL") 
            {$ClientName = "London Councils Ltd"}
           IF($ClientID -eq "LOWC") 
            {$ClientName = "Low Carbon Contracts Company"}
           IF($ClientID -eq "lowcarboncontracts.uk") 
            {$ClientName = "Low Carbon Contracts Company"}
           IF($ClientID -eq "MEC") 
            {$ClientName = "Mountain Equipment Co-op"}
           IF($ClientID -eq "mec.ca") 
            {$ClientName = "Mountain Equipment Co-op"}
           IF($ClientID -eq "MERG") 
            {$ClientName = "Mergent"}
           IF($ClientID -eq "mergent.com") 
            {$ClientName = "Mergent"}
           IF($ClientID -eq "MEYE") 
            {$ClientName = "Middle East Eye"}
           IF($ClientID -eq "middleeasteye.org") 
            {$ClientName = "Middle East Eye"}
           IF($ClientID -eq "MLSE.com") 
            {$ClientName = "Maple Leafs Sports & Entertainment"}
           IF($ClientID -eq "MLSE") 
            {$ClientName = "Maple Leafs Sports & Entertainment"}
           IF($ClientID -eq "MOBU") 
            {$ClientName = "Mobulus BV - Zeelenberg"}
           IF($ClientID -eq "NATU") 
            {$ClientName = "Natuurmonumenten"}
           IF($ClientID -eq "navis.com") 
            {$ClientName = "Navis"}
           IF($ClientID -eq "NAVS") 
            {$ClientName = "Navis"}
           IF($ClientID -eq "NECT") 
            {$ClientName = "Nectagen"}
           IF($ClientID -eq "NHSBT") 
            {$ClientName = "NHS Blood and Transplant"}
           IF($ClientID -eq "nhsbt.nhs.uk") 
            {$ClientName = "NHS Blood and Transplant"}
           IF($ClientID -eq "NSMMJIRA") 
            {$ClientName = "National September11 Memorial & Museum (Atlassian)"}
           IF($ClientID -eq "NVAA") 
            {$ClientName = "N-VA"}
           IF($ClientID -eq "OPER") 
            {$ClientName = "Open Referral"}
           IF($ClientID -eq "OPGP") 
            {$ClientName = "Open Group"}
           IF($ClientID -eq "OXFA") 
            {$ClientName = "Oxfam"}
           IF($ClientID -eq "PENT") 
            {$ClientName = "Pentair"}
           IF($ClientID -eq "pentair.com") 
            {$ClientName = "Pentair"}
           IF($ClientID -eq "PEOI") 
            {$ClientName = "PEO International"}
           IF($ClientID -eq "PETN") 
            {$ClientName = "Pet Health Network"}
           IF($ClientID -eq "pethealthinc.com") 
            {$ClientName = "Pet Health Network"}
           IF($ClientID -eq "PETS") 
            {$ClientName = "Pets Plus Us"}
           IF($ClientID -eq "petsplusus.com") 
            {$ClientName = "Pets Plus Us"}
           IF($ClientID -eq "cfinspet.com") 
            {$ClientName = "Pets Plus Us"}
           IF($ClientID -eq "PFZ") 
            {$ClientName = "Pfizer"}
           IF($ClientID -eq "phoenix.edu") 
            {$ClientName = "University of Phoenix"}
           IF($ClientID -eq "PIDP") 
            {$ClientName = "Pidpa"}
           IF($ClientID -eq "PLAN") 
            {$ClientName = "Plan International UK"}
           IF($ClientID -eq "PMI") 
            {$ClientName = "Project Management Institute"}
           IF($ClientID -eq "pmi.org") 
            {$ClientName = "Project Management Institute"}
           IF($ClientID -eq "POOL") 
            {$ClientName = "Poolcorp"}
           IF($ClientID -eq "Pool Corp") 
            {$ClientName = "Poolcorp"}
           IF($ClientID -eq "poolcorp.com") 
            {$ClientName = "Poolcorp"}
           IF($ClientID -eq "PROC") 
            {$ClientName = "ProCharger"}
           IF($ClientID -eq "PROS") 
            {$ClientName = "ProSolution"}
           IF($ClientID -eq "prosolution.com") 
            {$ClientName = "ProSolution"}
           IF($ClientID -eq "PROV") 
            {$ClientName = "Proviron Holding NV"}
           IF($ClientID -eq "PRUD") 
            {$ClientName = "Prudential"}
           IF($ClientID -eq "prudential.com.hk") 
            {$ClientName = "Prudential"}
           IF($ClientID -eq "Rimi") 
            {$ClientName = "Rimini Street"}
           IF($ClientID -eq "RKYM") 
            {$ClientName = "Rocky Mountaineer"}
           IF($ClientID -eq "rockymountaineer.com") 
            {$ClientName = "Rocky Mountaineer"}
           IF($ClientID -eq "royalroads.ca") 
            {$ClientName = "Royal Roads University"}
           IF($ClientID -eq "ROYR") 
            {$ClientName = "Royal Roads University"}
           IF($ClientID -eq "SCCU") 
            {$ClientName = "Swire Coca-Cola, USA"}
           IF($ClientID -eq "swirecc.com") 
            {$ClientName = "Swire Coca-Cola, USA"}
           IF($ClientID -eq "SCFHP") 
            {$ClientName = "Santa Clara Family Health Plan"}
           IF($ClientID -eq "scfhp.com") 
            {$ClientName = "Santa Clara Family Health Plan"}
           IF($ClientID -eq "SCVW") 
            {$ClientName = "Santa Clara Valley Water District"}
           IF($ClientID -eq "valleywater.org") 
            {$ClientName = "Santa Clara Valley Water District"}
           IF($ClientID -eq "SENS") 
            {$ClientName = "Sensoa vzw"}
           IF($ClientID -eq "sesame.org") 
            {$ClientName = "Sesame Workshop"}
           IF($ClientID -eq "SESM") 
            {$ClientName = "Sesame Workshop"}
           IF($ClientID -eq "SHOW") 
            {$ClientName = "Showtex"}
           IF($ClientID -eq "SKAP") 
            {$ClientName = "Simon-Kucher & Partners"}
           IF($ClientID -eq "simon-kucher.com") 
            {$ClientName = "Simon-Kucher & Partners"}
           IF($ClientID -eq "SINO") 
            {$ClientName = "Sino Group (HK)"}
           IF($ClientID -eq "SITC") 
            {$ClientName = "Singapore Institute of Technology"}
           IF($ClientID -eq "SNOF") 
            {$ClientName = "Snofolio"}
           IF($ClientID -eq "snofolio.com") 
            {$ClientName = "Snofolio"}
           IF($ClientID -eq "SOFT") 
            {$ClientName = "Softbank Robotics"}
           IF($ClientID -eq "SRP") 
            {$ClientName = "Superior Recreational Products"} 
		   IF($ClientID -eq "siibrands.com") 
            {$ClientName = "Superior Recreational Products"}
           IF($ClientID -eq "SUMI") 
            {$ClientName = "Sumitomo"}
           IF($ClientID -eq "surreyschools.ca") 
            {$ClientName = "Surrey Schools"}
           IF($ClientID -eq "SURS") 
            {$ClientName = "Surrey Schools"}
           IF($ClientID -eq "SUZU") 
            {$ClientName = "Suzuki Belgium"}
           IF($ClientID -eq "suzuki.be") 
            {$ClientName = "Suzuki Belgium"}
           IF($ClientID -eq "SWFT") 
            {$ClientName = "Swift"}
           IF($ClientID -eq "SYMP") 
            {$ClientName = "Antwerp Symphony Orchestra VZW"}
           IF($ClientID -eq "SYNB") 
            {$ClientName = "Syntra Bizz"}
           IF($ClientID -eq "SYNM") 
            {$ClientName = "SYNTRA Midden-Vlaanderen"}
           IF($ClientID -eq "TBTE") 
            {$ClientName = "The Basketball Tournament"}
           IF($ClientID -eq "thetournament.com") 
            {$ClientName = "The Basketball Tournament"}
		   IF($ClientID -eq "tcl.com") 
            {$ClientName = "TCL USA"}
           IF($ClientID -eq "TCLU") 
            {$ClientName = "TCL USA"}
           IF($ClientID -eq "TELE") 
            {$ClientName = "Telegraph"}
           IF($ClientID -eq "telegraph.co.uk") 
            {$ClientName = "Telegraph"}
           IF($ClientID -eq "TELUS") 
            {$ClientName = "TELUS"}
           IF($ClientID -eq "telus.com") 
            {$ClientName = "TELUS"}
           IF($ClientID -eq "TFALL") 
            {$ClientName = "Teach For All"}
           IF($ClientID -eq "teachforall.org") 
            {$ClientName = "Teach For All"}
		   IF($ClientID -eq "THOM") 
            {$ClientName = "Thomas More Mechelen"}
           IF($ClientID -eq "UBC") 
            {$ClientName = "UBC"}
		   IF($ClientID -eq "UCAL") 
            {$ClientName = "University of California"}
           IF($ClientID -eq "ucop.edu") 
            {$ClientName = "University of California"}
           IF($ClientID -eq "UCBE") 
            {$ClientName = "UC Berkeley"}
           IF($ClientID -eq "UCLA") 
            {$ClientName = "UCLA Unex"}
           IF($ClientID -eq "UDI") 
            {$ClientName = "Urban Development Institute"}
           IF($ClientID -eq "udi.org") 
            {$ClientName = "Urban Development Institute"}
           IF($ClientID -eq "UKGC") 
            {$ClientName = "UK G Cloud"}
           IF($ClientID -eq "UNIT") 
            {$ClientName = "Unity College"}	
		   IF($ClientID -eq "UOPX") 
            {$ClientName = "University of Phoenix"}
           IF($ClientID -eq "USMS") 
            {$ClientName = "US Masters Swimming"}
		   IF($ClientID -eq "UZAA") 
            {$ClientName = "UZA"}
           IF($ClientID -eq "VCAL") 
            {$ClientName = "Visit California"}
		   IF($ClientID -eq "visitcalifornia.com") 
            {$ClientName = "Visit California"}
           IF($ClientID -eq "VEGA") 
            {$ClientName = "Vega"}
           IF($ClientID -eq "voa.gsi.gov.uk") 
            {$ClientName = "Valuation Office Agency"}
           IF($ClientID -eq "VOAG") 
            {$ClientName = "Valuation Office Agency"}
           IF($ClientID -eq "VOKA") 
            {$ClientName = "Voka"}
           IF($ClientID -eq "VOPK") 
            {$ClientName = "Vopak"}
           IF($ClientID -eq "WADA") 
            {$ClientName = "World Anti Doping Agency"}
           IF($ClientID -eq "wada-ama.org") 
            {$ClientName = "World Anti Doping Agency"}
           IF($ClientID -eq "WATE") 
            {$ClientName = "Water-link"}
           IF($ClientID -eq "WEOR") 
            {$ClientName = "WE.org"}
		   IF($ClientID -eq "WEST") 
            {$ClientName = "Westfield Bank"}
           IF($ClientID -eq "westfield-bank.com") 
            {$ClientName = "Westfield Bank"}
		   IF($ClientID -eq "WINE") 
            {$ClientName = "Discover California Wine"}
           IF($ClientID -eq "WKCD") 
            {$ClientName = "WKCDA"}
           IF($ClientID -eq "WONI") 
            {$ClientName = "Woningen Blavier"}
           IF($ClientID -eq "YETI") 
            {$ClientName = "Yeti"}
           IF($ClientID -eq "yeti.com") 
            {$ClientName = "Yeti"}
           IF($ClientID -eq "ZETA") 
            {$ClientName = "Zeta Global"}
           IF($ClientID -eq "zetaglobal.com") 
            {$ClientName = "Zeta Global"}
           IF($ClientID -eq "zna.be") 
            {$ClientName = "ZNA"}
           IF($ClientID -eq "ZNAA") 
            {$ClientName = "ZNA"}
           IF($ClientID -eq "Agentschap Innoveren &amp; Ondernemen") 
            {$ClientName = "Agentschap Innoveren & Ondernemen"}
           IF($ClientID -eq "vlaio.be") 
            {$ClientName = "Agentschap Innoveren & Ondernemen"}
           IF($ClientID -eq "**MISSING from JSD**") 
            {$ClientName = "Belgische vereniging voor Urologie - BV"}
           IF($ClientID -eq "Zuiddag") 
            {$ClientName = "Zuiddag vzw"}
           IF($ClientID -eq "Zuiddag vzw") 
            {$ClientName = "Zuiddag vzw"} 			

        write-host "Creating Org: $ClientName | $ClientID"
            [String]$CompanyBody  = '{"name":"'+$ClientName+'"}'
            #$CBODY = $CompanyBody | ConvertTo-JSON
            [String] $target = $JIRAURL;
            $OrgCreateURI = $target+'/rest/servicedeskapi/organization'

            try {
                    $headers = @{
                        "Authorization" = $auth
                        "Content-Type"="application/json"
                        "X-ExperimentalApi"="opt-in"
                        "X-Atlassian-Token"="nocheck"
                        }
                        $createOrg = Invoke-RestMethod -Uri $OrgCreateURI -Method POST -Headers $headers -UseBasicParsing -Body $CompanyBody

            }
        catch {
            Write-Warning "Remote Server Response: $($_.Exception.Message)"
            Write-Output "Status Code: $($_.Exception.Response.StatusCode)"
        }

         #Add Org to Service Desk
         $JSDID = 2
         write-host "Adding Org to JSD, OrgID: $createOrg.id"
                    [String]$OrgIDBody  = '{"organizationId":'+$createOrg.id+'}'
                    $AddOrgURI = $target+'/rest/servicedeskapi/servicedesk/'+$JSDID+'/organization'
                     try {
                      $headers = @{
                        "Authorization" = $auth
                        "Content-Type"="application/json"
                        "X-ExperimentalApi"="opt-in"
                         "X-Atlassian-Token"="nocheck"
                         }
                        $AddOrg = Invoke-RestMethod -Uri $AddOrgURI -Method POST -Headers $headers -UseBasicParsing -Body $OrgIDBody
                    }
                    catch {
                        Write-Warning "Remote Server Response: $($_.Exception.Message)"
                        Write-Output "Status Code: $($_.Exception.Response.StatusCode)"
                        }

        $CTO++
    }
    UNTIL($CTO -eq $ClientArray.count)


######################################

<#
$Manual = 0
IF($manual -eq 0)
{
#Test export to CSV to scrub data for characters
$ContactArray = @()
$ce1 = 0
DO{
    write-host "processing: $ce1 of $C1"
    #$TicketArray.contactEmailAddress[$ce1]+" | "+ $TicketArray.contactName[$ce1] | out-file -file Contact_export.csv -append
    #$ContactArray += $TicketArray.contactEmailAddress[$ce1]+" | "+ $TicketArray.contactName[$ce1]
     $ContactArray = (get-content -path Contact_export.csv)
    $ce1++
}
UNTIL ($ce1 -eq $C1)


    ###################################################
    
    $CAL = $ContactArray | measure -Line
    $CI1 = 0
    DO{
        $CWContactName = $ContactArray[$CI1]  -creplace '^[^\\]*\| ', ''
        $CWcontactEmailAddress = $ContactArray[$CI1]  -creplace ' .*|', ''
        write-host "Creating record for: $CWcontactEmailAddress, CWContactName"

        [String]$body13 = '{\"email\":\"'+$CWcontactEmailAddress+'\",\"fullName\":\"'+$CWContactName+'\"}'
            $b64 = ConvertTo-Base64($username + ":" + $password);
            $auth = "Basic " + $b64;
            [String] $target = $JIRAURL;
            [String] $username = $JIRAUser;
            [String] $password = $JIRAPasswd;

                $ACMD = ' -i -u '+$username+':'+$password+' -H "Content-Type: application/json" -H "X-Atlassian-Token: nocheck" -H "X-ExperimentalApi: opt-in" -X POST -d "'+$body13+'" '+$target+'/rest/servicedeskapi/customer'
                Start-Process  $CURL $ACMD
            $CI1++
            }
            UNTIL($CI1 -eq $CAL.lines)
            

    ###################################################

     $ClientArray[25]

    $CTO = 0
    DO{
         ###################################################
            #Add Contact or Org after Creating
            #For Manual Runs Only, Comment out for production:
            #$CWcontactEmailAddress = "unknownuser@no-reply.com"
            $CWContactName = $ContactArray[$CTO]  -creplace '^[^\\]*\| ', ''
            $CWcontactEmailAddress = $ContactArray[$CTO]  -creplace ' .*|', ''
            $ClientDomain = $CWcontactEmailAddress -creplace '(?s)^.*@', ''
            $CCID = $Orgs -match $ClientDomain -creplace  '.*@ ', ''
            write-host "Adding users $CWcontactEmailAddress to org $ClientDomain"
            #$CCID = 919
            #END MANUAL RUN
            [String]$body14 = '{\"usernames\": [\"'+$CWcontactEmailAddress+'\"]}'
            $b64 = ConvertTo-Base64($username + ":" + $password);
            $auth = "Basic " + $b64;
            [String] $target = $JIRAURL;
            [String] $username = $JIRAUser;
            [String] $password = $JIRAPasswd;

                $ACMD = ' -i -u '+$username+':'+$password+' -H "Content-Type: application/json" -H "X-Atlassian-Token: nocheck" -H "X-ExperimentalApi: opt-in" -X POST -d "'+$body14+'" '+$target+'/rest/servicedeskapi/organization/'+$CCID+'/user'
                Start-Process  $CURL $ACMD
        $CTO++
    }
    UNTIL($CTO -eq $CAL.lines)



    }


IF($debug -lt 2)
{
write-host "Debug enabled, pausing"
write-host "You are about to start Debug testing"
write-host "This will start to create Orgs and Contacts within JSD"

    $i1 = 0
    DO {

    
    IF ($Token1 -eq 2000)
    {
    #Ensure CW token stays valid:
            [string]$BaseUri     = "$CWServerRoot" + "v4_6_release/apis/3.0/system/members/$ImpersonationMember/tokens"
            [string]$Accept      = "application/vnd.connectwise.com+json; version=v2018_5"
            [string]$ContentType = "application/json"
            #[string]$Authstring  = $CWInfocompany + '+' + $CWInfouser + ':' + $CWInfopassword
            [string]$Authstring  = $CWInfocompany + '+' + $Public + ':' + $Private

        #Convert the user and pass (aka public and private key) to base64 encoding
        $encodedAuth = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(($Authstring)));

        #Create Header
        $header = [Hashtable] @{
            Authorization = ("Basic {0}" -f $encodedAuth)
            Accept        = $Accept
            Type          = "application/json"
            #'x-cw-usertype' = "integrator"
            'x-cw-usertype' = "member" 
        };

        $body   = "{`"memberIdentifier`":`"$ImpersonationMember`"}"
        #execute the the request
        $response = Invoke-RestMethod -Uri $Baseuri -Method Post -Headers $header -Body $body -ContentType $contentType;

        $CWcredentials = $response
            $Headers=@{
            "Authorization"="Basic $encodedAuth"
            }
    #END OF CW TOKEN
    $Token1 = 0
    }
    ELSE
    {
    $Token1++
    }

        #Pull out variables from JSON results
        $CWID = $TicketArray.id[$i1]
        $CWSummary = $TicketArray.summary[$i1]
        $CWContactName = $TicketArray.Contactname[$i1]
        $CWCompany = $TicketArray.company.identifier[$i1]
        $CWSTatus = $TicketArray.status.name[$i1]
        $CWBoard = $TicketArray.board.name[$i1]
        $CWCompany = $TicketArray.company.identifier[$i1]
        $CWBudgetHours = $TicketArray.budgethours[$i1]
        $CWBudgetHoursI = [int]$CWBudgetHours
        $CWBudgetMin = $CWBudgetHoursI*60
        $CWSpentHours = $TicketArray.actualHours[$i1]
        $CWSpentHoursI = [int]$CWSpentHours
        $CWSpentMin = $CWSpentHoursI*60
        $CWPriority = $TicketArray.priority.name[$i1]
        $CWDateEntered = $TicketArray.DateEntered[$i1]
        $CWDateClosed = $TicketArray.closeddate[$i1]
        $CWEnteredBy = $TicketArray.Enteredby[$i1]
        #################################
        #Scrub domain from email address.
        $CWcontactEmailAddress = $TicketArray.contactEmailAddress[$i1]
        $ClientDomain = $CWcontactEmailAddress -creplace '(?s)^.*@', ''
        #Display whom is being created
        write-host "Creating Contact For"
        $CWContactName
        $CWcontactEmailAddress
        $ClientDomain
        IF ($Orgs -match $ClientDomain)
        {
        $OrgPresent = "True"
        #Create Contact
            IF(!$CWContactName)
            {
            write-host "Missing Customer Name" 
            $CWContactName = $CWcontactEmailAddress -creplace '@.*', '' ` -replace "[^a-zA-Z0-9_]", " "
                IF(!$CWContactName)
                {
                $CWContactName = "anonymous user"
                }
            }
            IF(!$CWcontactEmailAddress)
            {
            $CWcontactEmailAddress = "unknownuser@no-reply.com"
            $ClientDomain = $CWCompany
            }
            $CCID = $Orgs -match $ClientDomain -creplace  '.*@ ', ''
            ###################################################
            #See if contact exists in system
            $CC = 0
            $Contacts = @()
            $ContactNames = @()
            DO{

                        [String] $target = $JIRAURL;
                        [String] $username = $JIRAUser
                        [String] $password = $JIRAPasswd
                        [String] $start = "start="+$CC
                        [String] $limit = "limit=50"
                        $requestUri = $target+"/rest/servicedeskapi/organization/$CCID/user?$start&$limit"

                                try {
                                    $headers = @{
                                        "Authorization" = $auth
                                        "Content-Type"="application/json"
                                        "X-ExperimentalApi"="opt-in"
                                    }
                                    $OrgInfo = Invoke-RestMethod -Uri $requestUri -Method GET -Headers $headers -UseBasicParsing

                                    }
                                catch {
                                    Write-Warning "Remote Server Response: $($_.Exception.Message)"
                                    Write-Output "Status Code: $($_.Exception.Response.StatusCode)"
                                }

                            $Lines = $OrgInfo.Values.name  | Measure -Line
                            $LC = 0
                        DO{
                            IF(!$OrgInfo.values)
                            {
                            write-host "No users in query"
                            $ContactNames = 0
                            }
                            ELSE
                            {
                            $ContactNames += $OrgInfo.Values.Name[$LC]
                            }
                            $LC++
                        }
                        UNTIL($LC -gt $Lines.Lines)

                        $MORE = $OrgInfo.isLastPage
                        $CC+=50
                }
                UNTIL ($MORE -eq "True")

                IF(!$ContactNames)
                {
                write-host "No contacts within Org"
                }
                ELSE
                {
                IF($ContactNames -match $CWcontactEmailAddress)
                {
                write-host "Contact is already present"
                }
                ELSE
                    {
                    write-host "Contact is missing, creating now"

                            #Create Contact
                                [String]$body13 = '{\"email\":\"'+$CWcontactEmailAddress+'\",\"fullName\":\"'+$CWContactName+'\"}'
                                $b64 = ConvertTo-Base64($username + ":" + $password);
                                $auth = "Basic " + $b64;
                                [String] $target = $JIRAURL;
                                [String] $username = $JIRAUser;
                                [String] $password = $JIRAPasswd;

                                $ACMD = ' -i -u '+$username+':'+$password+' -H "Content-Type: application/json" -H "X-Atlassian-Token: nocheck" -H "X-ExperimentalApi: opt-in" -X POST -d "'+$body13+'" '+$target+'/rest/servicedeskapi/customer'
                                Start-Process  $CURL $ACMD

   
                                        ###################################################
                                        #Add Contact or Org after Creating
                                        #For Manual Runs Only, Comment out for production:
                                        #$CWcontactEmailAddress = "unknownuser@no-reply.com"
                                        $ClientDomain = $CWcontactEmailAddress -creplace '(?s)^.*@', ''
                                        $CCID = $Orgs -match $ClientDomain -creplace  '.*@ ', ''
                                        #$CCID = 919
                                        #END MANUAL RUN
                                        [String]$body14 = '{\"usernames\": [\"'+$CWcontactEmailAddress+'\"]}'
                                        $b64 = ConvertTo-Base64($username + ":" + $password);
                                        $auth = "Basic " + $b64;
                                        [String] $target = $JIRAURL;
                                        [String] $username = $JIRAUser;
                                        [String] $password = $JIRAPasswd;

                                        $ACMD = ' -i -u '+$username+':'+$password+' -H "Content-Type: application/json" -H "X-Atlassian-Token: nocheck" -H "X-ExperimentalApi: opt-in" -X POST -d "'+$body14+'" '+$target+'/rest/servicedeskapi/organization/'+$CCID+'/user'
                                        Start-Process  $CURL $ACMD
                    }
                }

        }
        #Org is not present, must be created first
        ELSE
        {
        $OrgPresent = "False"
        #Create Things
        $JSDID = 1
            ###################################################
            #Create the Org
            [String]$body11  = '{\"name\":\"'+$ClientDomain+'\"}'
            [String] $target = $JIRAURL;
            [String] $username = $JIRAUser;
            [String] $password = $JIRAPasswd;


            $ACMD = ' -i -u '+$username+':'+$password+' -H "Content-Type: application/json" -H "X-Atlassian-Token: nocheck" -H "X-ExperimentalApi: opt-in" -X POST -d '+$body11+' '+$target+'/rest/servicedeskapi/organization'
            Start-Process  $CURL  $ACMD

            
            try {
                $headers = @{
                            "Authorization" = $auth
                            "Content-Type"="application/json"
                            "X-ExperimentalApi"="opt-in"
                            }
            $requestURI = "$target/rest/servicedeskapi/organization"
            $response = Invoke-RestMethod -Uri $requestUri -Method POST -Headers $headers -Body $body11 -UseBasicParsing
            }
            catch {
                        Write-Warning "Remote Server Response: $($_.Exception.Message)"
                        Write-Output "Status Code: $($_.Exception.Response.StatusCode)"
                    }
            #Repull all orgs to get OrgID
            $NR = 0
            $OC = 0
            $Orgs = @()
            $OrgID = @()
            $OrgName = @()
            DO{

                    [String] $target = $JIRAURL;
                    [String] $username = $JIRAUser
                    [String] $password = $JIRAPasswd
                    [String] $start = "start="+$NR
                    [String] $limit = "limit=50"
                    $requestUri = $target+"/rest/servicedeskapi/organization?$start&$limit"

                            try {
                                $headers = @{
                                    "Authorization" = $auth
                                    "Content-Type"="application/json"
                                    "X-ExperimentalApi"="opt-in"
                                }
                                $OrgInfo = Invoke-RestMethod -Uri $requestUri -Method GET -Headers $headers -UseBasicParsing

                                }
                            catch {
                                Write-Warning "Remote Server Response: $($_.Exception.Message)"
                                Write-Output "Status Code: $($_.Exception.Response.StatusCode)"
                            }

                        $Lines = $OrgInfo.Values.name  | Measure -Line
                        $OrgID += $OrgInfo.Values.id
                        $OrgName += $OrgInfo.Values.Name
                        $LC = 0
                    DO{
                        $ID = $OrgInfo.Values.id[$LC]
                        $NAME = $OrgInfo.Values.Name[$LC]
                        $Orgs += $NAME +" @ " +$ID
                        $LC++
                    }
                    UNTIL($LC -eq $Lines.Lines)

                    $MORE = $OrgInfo.isLastPage
                    $NR+=50
            }
            UNTIL ($MORE -eq "True")
            $CCID = $Orgs -match $ClientDomain -creplace  '.*@ ', ''
                    ###################################################
                    #Add Org to Service Desk
                    [String]$body12  = '{\"organizationId\":'+$CCID+'}'
                    $b64 = ConvertTo-Base64($username + ":" + $password);
                    $auth = "Basic " + $b64;
                    [String] $target = $JIRAURL;
                    [String] $username = $JIRAUser;
                    [String] $password = $JIRAPasswd;

                    $ACMD = ' -i -u '+$username+':'+$password+' -H "Content-Type: application/json" -H "X-Atlassian-Token: nocheck" -H "X-ExperimentalApi: opt-in" -X POST -d "'+$body12+'" '+$target+'/rest/servicedeskapi/servicedesk/'+$JSDID+'/organization'
                    Start-Process  $CURL $ACMD

                        ###################################################
                        #Create Contact
                            [String]$body13 = '{\"email\":\"'+$CWcontactEmailAddress+'\",\"fullName\":\"'+$CWContactName+'\"}'
                            $b64 = ConvertTo-Base64($username + ":" + $password);
                            $auth = "Basic " + $b64;
                            [String] $target = $JIRAURL;
                            [String] $username = $JIRAUser;
                            [String] $password = $JIRAPasswd;

                            $ACMD = ' -i -u '+$username+':'+$password+' -H "Content-Type: application/json" -H "X-Atlassian-Token: nocheck" -H "X-ExperimentalApi: opt-in" -X POST -d "'+$body13+'" '+$target+'/rest/servicedeskapi/customer'
                            Start-Process  $CURL $ACMD

                                    ###################################################
                                    #Add Contact or Org after Creating

                                    [String]$body14 = '{\"usernames\": [\"'+$CWcontactEmailAddress+'\"]}'
                                    $b64 = ConvertTo-Base64($username + ":" + $password);
                                    $auth = "Basic " + $b64;
                                    [String] $target = $JIRAURL;
                                    [String] $username = $JIRAUser;
                                    [String] $password = $JIRAPasswd;

                                    $ACMD = ' -i -u '+$username+':'+$password+' -H "Content-Type: application/json" -H "X-Atlassian-Token: nocheck" -H "X-ExperimentalApi: opt-in" -X POST -d "'+$body14+'" '+$target+'/rest/servicedeskapi/organization/'+$CCID+'/user'
                                    Start-Process  $CURL $ACMD

        }

        #################################
        #END Create Customer and Contacts
        #################################


        write-host "$i1 out of $C1"
            $i1++
    }
    #FOR DEBUGGING, COMMENT OUT FOR ACTUAL USE
    #UNTIL ($i1 -eq 100)
    UNTIL ($i1 -eq $C1)
    write-host ""



}
#>