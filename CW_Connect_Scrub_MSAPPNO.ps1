
##################
#Enable debugging
#0 = Disabled
#1 = Enabled
#2 = Recursive logging, no delete
##################
$debug = 1
IF($debug -lt 2)
{
remove-item C:\ContractWork\Appno\debug.log
}
ELSE
{
echo Log not deleted, Recursive logging enabled
}



cls
#Pickup Creds from secure file
$user = (Get-Content C:\ContractWork\Appno\security_file.secure)[1]
$pwdencrypt = (Get-Content C:\ContractWork\Appno\security_file.secure)[3]
$url = (Get-Content C:\ContractWork\Appno\security_file.secure)[5]
$Public = (Get-Content C:\ContractWork\Appno\security_file.secure)[7]
$Private = (Get-Content C:\ContractWork\Appno\security_file.secure)[9]
$JIRAURL = (Get-Content C:\ContractWork\Appno\security_file.secure)[11]
$JIRAUser = (Get-Content C:\ContractWork\Appno\security_file.secure)[13]
$JIRAPasswd = (Get-Content C:\ContractWork\Appno\security_file.secure)[15]

function ConvertTo-Base64($string) {
    $bytes = [System.Text.Encoding]::UTF8.GetBytes($string);
    $encoded = [System.Convert]::ToBase64String($bytes);
    return $encoded;
    }

$Token1 = 0

$ImpersonationMember = 'acceleratetemp'

$CURL = "C:\Windows\System32\curl.exe"

#Encrypt password to base64string
#$encodedAuth = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(('passwordhere')));

#Comment out if password is plain txt
#$encodedAuthDeCrypt =  [System.Text.Encoding]::ASCII.GetString([System.Convert]::FromBase64String($pwdencrypt))

#Uncomment if password is plain txt
$encodedAuthDeCrypt = $pwdencrypt


#API pre-authentication
$CWServerRoot = $url
$CWInfocompany = 'appno'
$CWInfouser = $user
$CWInfopassword = $encodedAuthDeCrypt

cls

    [string]$BaseUri     = "$CWServerRoot" + "v4_6_release/apis/3.0/system/members/$ImpersonationMember/tokens"
    [string]$Accept      = "application/vnd.connectwise.com+json; version=v2017_2"
    [string]$ContentType = "application/json"
    #[string]$Authstring  = $CWInfocompany + '+' + $CWInfouser + ':' + $CWInfopassword
    [string]$Authstring  = $CWInfocompany + '+' + $Public + ':' + $Private

    #Convert the user and pass (aka public and private key) to base64 encoding
    $encodedAuth = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(($Authstring)));

    #Create Header
    $header = [Hashtable] @{
        Authorization = ("Basic {0}" -f $encodedAuth)
        Accept        = $Accept
        Type          = "application/json"
        #'x-cw-usertype' = "integrator"
        'x-cw-usertype' = "member" 
    };

    $body   = "{`"memberIdentifier`":`"$ImpersonationMember`"}"

    #execute the the request
    $response = Invoke-RestMethod -Uri $Baseuri -Method Post -Headers $header -Body $body -ContentType $contentType;

    $CWcredentials = $response

IF($debug -lt 2)
{
write-host "Debug enabled, pausing"
write-host "So far so good is no errors were thrown, shall we proceed?"
pause
}
ELSE
{

}




#API Access post authentication    
    [string]$Accept      = "application/vnd.connectwise.com+json; version=v2018_4"
    [string]$ContentType = "application/json"
    [string]$Authstring = "$CWInfocompany+" + $($CWcredentials.publickey) + ':' + $($CWCredentials.privatekey)
    $encodedAuth = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(($Authstring)));
#Ticket URL
    [string]$BaseUri     = "$CWServerRoot" + "v4_6_Release/apis/3.0/service/tickets?pagesize=100"
#Ticket DEBUG URL
    [string]$BaseUriDEBUG     = "$CWServerRoot" + "v4_6_Release/apis/3.0/service/tickets?pagesize=10"
#Ticket Notes
    [string]$BaseNoteUri = "$CWServerRoot" + "v4_6_release/apis/3.0/service/tickets/$Ticket/notes"
#Ticket Attachments
    [string]$BaseAttchUri = "$CWServerRoot" + "v4_6_release/apis/3.0/system/documents?recordType=Ticket&recordId=$Ticket"
#Ticket URL for single ticket
    #    [string]$BaseUriT     = "$CWServerRoot" + "v4_6_Release/apis/3.0/service/tickets/$Ticket"
#Ticket Count
    [string]$BaseUriTC    = "$CWServerRoot" + "v4_6_Release/apis/3.0/service/tickets/count"
#Attachment URL
    [string]$AttachmentUri = "$CWServerRoot" + "v4_6_release/apis/3.0/service/tickets/$Ticket/documents"
#$Summary = $Summary.substring(0,75)

IF($debug -lt 2)
{
write-host "Debug enabled, pausing"
write-host "You are about to start Debug testing"

#Uncomment the following for single ticket testing and JSON readback.
<#
##Currently Testing
$Ticket = 1500

#Ticket DEBUG URL
    [string]$BaseUriDEBUG     = "$CWServerRoot" + "v4_6_Release/apis/3.0/service/tickets/$Ticket"
#Ticket Notes
    [string]$BaseNoteUri = "$CWServerRoot" + "v4_6_release/apis/3.0/service/tickets/$Ticket/notes"
#Ticket Attachments
    [string]$BaseAttchUri = "$CWServerRoot" + "v4_6_release/apis/3.0/system/documents?recordType=Ticket&recordId=$Ticket"

    #CW Ticket Debug Info
    $TicketInfo = Invoke-RestMethod -URI $BaseURIDEBUG -Headers $Headers -ContentType $ContentType -Method GET

    #CW Ticket Note Info
    $TicketNote = Invoke-RestMethod -URI $BaseNoteUri -Headers $Headers -ContentType $ContentType -Method GET

    #CW Ticket Attch Info
    $TicketAttachment = Invoke-RestMethod -URI $BaseAttchUri -Headers $Headers -ContentType $ContentType -Method GET
#>

pause
}
ELSE
{

}


#Body is ONLY needed to POST or PUT, this is NOT needed for GET calls
$Body= @"
{

}
"@
$Body

$Headers=@{
"Authorization"="Basic $encodedAuth"
}



    #CW Ticket Count Call
    $CWTicketCount = $JSONResponse = Invoke-RestMethod -URI $BaseURITC -Headers $Headers -ContentType $ContentType -Method GET
    $PageCount = ($CWTicketCount.Count/1000)+1

IF($debug -lt 2)
{
write-host "Debug enabled, pausing"
write-host "You are about to start the process to pull $CWTicketCount tickets"
write-host "This will be done 1000 tickets at a time and stored into a variable array within memory"
pause
}
ELSE
{

}

#Collect ALL tickets from Connectwise and store them within a variable array all within memory.
    $TicketArray = @()
    $JSONTicket = 0
    $tc = 1
    DO {
    [string]$IncTicUri = "$CWServerRoot" + "v4_6_Release/apis/3.0/service/tickets?page=$tc&pagesize=1000"
    $JSONTickets = Invoke-RestMethod -URI $IncTicUri -Headers $Headers -ContentType $ContentType -Method GET
    $TicketArray += $JSONTickets
    #$TicketArray.id
    $TicketArray.count
    $tc++
    }
    Until ($tc -gt $PageCount)


    $ObjectCount = $TicketArray | measure
    $C1 = $ObjectCount.count

IF($debug -lt 2)
{
write-host "Debug enabled, pausing"
write-host "You are about to start creating issues in Jira, think about it"
pause
}
ELSE
{

}

#Output clients to CSV file, only needed for import of client IDs/Names
#$TicketArray.company | select identifier, name | Export-csv "C:\ContractWork\Appno\ClientIDs.csv"

###################################################
###################################################
###################################################
###################################################
#Gather all of the present JSD Orgs
###################################################
###################################################
###################################################
###################################################
   
    $b64 = ConvertTo-Base64($username + ":" + $password);
    $auth = "Basic " + $b64;
$NR = 0
$OC = 0
$Orgs = @()
$OrgID = @()
$OrgName = @()


DO{

    [String] $target = $JIRAURL;
    [String] $username = $JIRAUser
    [String] $password = $JIRAPasswd
    [String] $start = "start="+$NR
    [String] $limit = "limit=50"
    $requestUri = $target+"/rest/servicedeskapi/organization?$start&$limit"

        try {
            $headers = @{
                "Authorization" = $auth
                "Content-Type"="application/json"
                "X-ExperimentalApi"="opt-in"
            }
            $OrgInfo = Invoke-RestMethod -Uri $requestUri -Method GET -Headers $headers -UseBasicParsing

            }
        catch {
            Write-Warning "Remote Server Response: $($_.Exception.Message)"
            Write-Output "Status Code: $($_.Exception.Response.StatusCode)"
        }
        $OrgCount = $OrgInfo.values | Measure-Object

    IF (($OrgCount.Count -eq 0))
        {
        $MORE = "True"
        write-host "No orgs present within JSD"
        }
     ELSE
        {
            $Lines = $OrgInfo.Values.name  | Measure -Line
            $OrgID += $OrgInfo.Values.id
            $OrgName += $OrgInfo.Values.Name
            $LC = 0
            DO{
            $ID = $OrgInfo.Values.id[$LC]
            $NAME = $OrgInfo.Values.Name[$LC]
            $Orgs += $NAME +" @ " +$ID
            $LC++
            }
            UNTIL($LC -eq $Lines.Lines)

            $MORE = $OrgInfo.isLastPage
            $NR+=50
        }
    }
    UNTIL ($MORE -eq "True")
    $OrgName | Measure -Line


###################################################
###################################################
###################################################
###################################################
#This process below is what creates the arrays and starts creating tickets in Jira
###################################################
###################################################
###################################################
###################################################


#Check for presence of Attachment directory
$AttachmentPath = "C:\ContractWork\Appno\Attachments"
if(!(Test-Path -Path $AttachmentPath)){
    
    write-host "Attachments directory is missing, creating is now to avoid errors"
    md "C:\ContractWork\Appno\Attachments"
    Remove-Item "C:\ContractWork\Appno\CurlAttachmentCall.txt"
}
else
{
    Remove-Item "C:\ContractWork\Appno\Attachments\*"
    Remove-Item "C:\ContractWork\Appno\CurlAttachmentCall.txt"
}


#Clear Total Attachment Variable Count before run
$TotalAttachmentCount = 0


    $i1 = 26950
    DO {

    
    IF ($Token1 -eq 2000)
    {
    #Ensure CW token stays valid:
            [string]$BaseUri     = "$CWServerRoot" + "v4_6_release/apis/3.0/system/members/$ImpersonationMember/tokens"
            [string]$Accept      = "application/vnd.connectwise.com+json; version=v2018_5"
            [string]$ContentType = "application/json"
            #[string]$Authstring  = $CWInfocompany + '+' + $CWInfouser + ':' + $CWInfopassword
            [string]$Authstring  = $CWInfocompany + '+' + $Public + ':' + $Private

        #Convert the user and pass (aka public and private key) to base64 encoding
        $encodedAuth = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(($Authstring)));

        #Create Header
        $header = [Hashtable] @{
            Authorization = ("Basic {0}" -f $encodedAuth)
            Accept        = $Accept
            Type          = "application/json"
            #'x-cw-usertype' = "integrator"
            'x-cw-usertype' = "member" 
        };

        $body   = "{`"memberIdentifier`":`"$ImpersonationMember`"}"
        #execute the the request
        $response = Invoke-RestMethod -Uri $Baseuri -Method Post -Headers $header -Body $body -ContentType $contentType;

        $CWcredentials = $response
            $Headers=@{
            "Authorization"="Basic $encodedAuth"
            }
    #END OF CW TOKEN
    $Token1 = 0
    }
    ELSE
    {
    $Token1++
    }
  

        #Progress Bar
        #$PC = ($i1/$C1)*100
        #{Write-Progress -Activity "Importing Tickets into Jira" -Status "$PC% Complete:" -PercentComplete $PC}


        #Pull out variables from JSON results
        $CWID = $TicketArray.id[$i1]
        $CWSTATUS = $TicketArray.status.name[$i1]
        $CWSummary = $TicketArray.summary[$i1]
        $CWContactName = $TicketArray.Contactname[$i1]
        $CWcontactEmailAddress = $TicketArray.contactEmailAddress[$i1]
        $CWSTatus = $TicketArray.status.name[$i1]
        $CWBoard = $TicketArray.board.name[$i1]
        $CWCompanyName = $TicketArray.company.Name[$i1]
        $CWCompany = $TicketArray.company.identifier[$i1]
        $CWBudgetHours = $TicketArray.budgethours[$i1]
        $CWBudgetHoursI = [int]$CWBudgetHours
        $CWBudgetMin = $CWBudgetHoursI*60
        $CWSpentHours = $TicketArray.actualHours[$i1]
        $CWSpentHoursI = [int]$CWSpentHours
        $CWSpentMin = $CWSpentHoursI*60
        #$CWPriority = $TicketArray.priority.name[$i1]
        $CWDateEntered = $TicketArray.DateEntered[$i1]
        $CWDateClosed = $TicketArray.closeddate[$i1]
        $CWEnteredBy = $TicketArray.Enteredby[$i1]
        $CWpriority = $TicketArray.priority.name[$i1] -creplace '(?s)^.* ', ''
		$CWOwner = $TicketArray.owner[$i1].identifier
        #################################

        
        #$CWSTatus 

        ##########################################
        ##########################################
        ##########################################
        #Mapping table 
            IF ($CWSTatus -eq "New from Triage") {$CWSTatus = "Import-In-triage"}
            IF ($CWSTatus -eq "New")	{$CWSTatus = "Import-In-triage"}
            #IF ($CWSTatus -eq "Closed Loop") {"NOT USED"}
            IF ($CWSTatus -eq "Pending Estimate")	{$CWSTatus = "Import-Estimation"}
            IF ($CWSTatus -eq "Estimate Rejected")	{$CWSTatus = "Import-Estimation"}
            IF ($CWSTatus -eq "Waiting for Approval")	{$CWSTatus = "Import-Waiting-for-Approval"}
            IF ($CWSTatus -eq "Client responded (Estimate)")	{$CWSTatus = "Import-Ready-for-work"}
            IF ($CWSTatus -eq "Waiting for PR/MR to be merged")	{$CWSTatus = "Import-In-progress"}
            IF ($CWSTatus -eq "MR/PR Merged")	{$CWSTatus = "Import-In-progress"}
            IF ($CWSTatus -eq "Ready for work")	{$CWSTatus = "Import-Ready-for-work"}
            IF ($CWSTatus -eq "In Progress")	{$CWSTatus = "Import-In-progress"}
            IF ($CWSTatus -eq "Waiting for QA")	{$CWSTatus = "Import-Waiting-for-QA"}
            IF ($CWSTatus -eq "QA Failed")	{$CWSTatus = "Import-In-progress"}
            IF ($CWSTatus -eq "Waiting for IT")	{$CWSTatus = "Import-In-progress"}
            IF ($CWSTatus -eq "QA Passed")	{$CWSTatus = "Import-Merge-UAT"}
            IF ($CWSTatus -eq "UAT Code Confirmation")	{$CWSTatus = "Import-Merge-UAT"}
            IF ($CWSTatus -eq "Waiting for UAT")	{$CWSTatus = "Import-Waiting-for-UAT"}
            IF ($CWSTatus -eq "Client responded (UAT)")	{$CWSTatus = "Import-Ready-for-deploy"}
            IF ($CWSTatus -eq "UAT Failed")	{$CWSTatus = "Import-In-progress"}
            IF ($CWSTatus -eq "Ready for Deployment")	{$CWSTatus = "Import-Ready-for-deploy"}
            IF ($CWSTatus -eq "Waiting for Client")	{$CWSTatus = "Import-Waiting-for-Client"}
            IF ($CWSTatus -eq "Waiting for Client2")	{$CWSTatus = "Import-Waiting-for-Client"}
            IF ($CWSTatus -eq "Waiting for Client final")	{$CWSTatus = "Import-Waiting-for-Client"}
            IF ($CWSTatus -eq "On Hold")	{$CWSTatus = "Import-On-Hold"}
            IF ($CWSTatus -eq "Waiting for Info")	{$CWSTatus = "Import-Waiting-for-Client"}
            IF ($CWSTatus -eq "Deployed to DEV (UCAL)")	{$CWSTatus = "Import-Completed"}
            IF ($CWSTatus -eq "Deployed to TEST (UCAL)")	{$CWSTatus = "Import-Completed"}
            IF ($CWSTatus -eq "Deployed to UAT (UCAL)")	{$CWSTatus = "Import-Completed"}
            IF ($CWSTatus -eq "Deployed to PROD (UCAL)")	{$CWSTatus = "Import-Completed"}
            IF ($CWSTatus -eq "Deployed - Waiting for confirmation")	{$CWSTatus = "Import-Completed"}
            IF ($CWSTatus -eq ">Closed")	{$CWSTatus = "Import-Closed"}
            IF ($CWSTatus -eq "Re-Opened")	{$CWSTatus = "Import-Ready-for-work"}
            IF ($CWSTatus -eq ">Cancelled")	{$CWSTatus = "Import-Cancelled"}
            IF ($CWSTatus -eq "Tracked in JIRA")	{$CWSTatus = "Import-In-progress"}

            #Priority Mapping
            #P1 -> impact = high, urgency = high
            #P2 -> impact = medium, urgency = high
            #P3 -> impact = medium, urgency = medium
            #P4 -> impact = low, urgency = medium
            #P5 -> impact = low, urgency = low
            IF ($CWPriority -eq 1)
                {
                $impact = 10303
                $urgency = 10300
                $priority = 10000
                }
            IF ($CWPriority -eq 2)
                {
                $impact = 10304
                $urgency = 10300
                $priority = 10100
                }
            IF ($CWPriority -eq 3)
                {
                $impact = 10304
                $urgency = 10301
                $priority = 10101
                }
            IF ($CWPriority -eq 4)
                {
                $impact = 10305
                $urgency = 10301
                $priority = 3
                }
            IF ($CWPriority -eq 5)
                {
                $impact = 10305
                $urgency = 10302
                $priority = 10001
                }

            

        <# Removed due to direct label translation requested
        IF($CWSTATUS -eq ">Closed")
        {
        $CWSTATUS = "ImportClosed"
        }
        ELSE
        {
        $CWSTATUS = "ImportOpen"
        }
        #>

        #Gather Notes and Attachments
        $Ticket = $CWID
        #Ticket Notes
        [string]$BaseNoteUri = "$CWServerRoot" + "v4_6_release/apis/3.0/service/tickets/$Ticket/notes"
        #Ticket Attachments
        #Not the right call for attachments on a ticket, its ok to cry a little over this
        #[string]$BaseAttchUri = "$CWServerRoot" + "v4_6_release/apis/3.0/system/documents?recordType=Ticket&recordId=$Ticket"
        [string]$BaseAttchUri = "$CWServerRoot" + "v4_6_release/apis/3.0/service/tickets/$Ticket/documents"
        #Ticket Time Entries (Internal Notes)
        [string]$BaseTimeUri = "$CWServerRoot" + "/v4_6_release/apis/3.0/service/tickets/$Ticket/timeentries"
        #API Calls for notes
        #CW Ticket Note Info
        $TicketNote = Invoke-RestMethod -URI $BaseNoteUri -Headers $Header -ContentType $ContentType -Method GET
        #$TicketNote.count
        #CW Ticket Attch Info
        #$TicketAttachment = Invoke-RestMethod -URI "https://eu.myconnectwise.net/v4_6_release/apis/3.0/service/tickets/27338/documents" -Headers $Headers -ContentType $ContentType -Method GET
        $TicketAttachment = Invoke-RestMethod -URI $BaseAttchUri -Headers $Header -ContentType $ContentType -Method GET
        #CW Ticket Internal Notes Info
        $TicketTime = Invoke-RestMethod -URI $BaseTimeUri -Headers $Header -ContentType $ContentType -Method GET

        #$TicketAttachment.count

        #Start counting attachments stored within Connectwise
        $TotalAttachmentCount += $TicketAttachment.count
		
		#Set Assignee to the default user so that if the user is not in the approved list, it uses the default
        #write-host "CWOwner: "
		$ApprovedUserList = "agrotskyi","anoorani","arsalan","chornstra","davidl","DEdwards","diegotejera","dsahoo","ECann","eduardb","EScott","GMendoza","hmarsland","Hserrano","imendoza","jacintorobles","jating","lexies","mabdelal","mhanlon","michaelp","MLagace","Nfadaeian","nivb","Racosta","rchatfield","RMuntasir","rsalazar","schu","SJoomratty","SPonzio","TKirby","commerce","isanchez","mbandaru","mmachado","mmoghaddam","rafaelo","rlagman"
		IF ($ApprovedUserList -contains $CWOwner)
		{
			#Assign ticket to owner from Connectwise if owner is in approved list
			$Assignee = $CWOwner
		}
        ELSE
        {
		    $Assignee = "cwuser" #Default user
        }

        #Create JIRA Tickets
        #Code was discovered on Reddit, vetted, tested and adjusted for this application.
        ##
        ##Need homes for:
        ##Board: $CWBoard, Entered: $CWDateEntered, Closed: $CWDateClosed, EnteredBy: $CWEnteredBy

         $CWSummary = $CWSummary -replace '"',""



    [String] $target = $JIRAURL;
    [String] $username = $JIRAUser
    [String] $password = $JIRAPasswd
    #[String] $projectKey = "TEST";
    [String] $projectKey = "MSAPPNO";
    [String] $issueType = "Problem";
    [String] $RequestType = "Report a Bug/Problem";
    [String] $Summary = $CWSummary;
    [String] $Client = $ClientDomain;
    #Uncomment only if there is an issue upon creation
    #$Summary = $Summary -replace "[^a-zA-Z0-9_.]", ""
    [String] $BudgetHours = $CWBudgetMin;
    [String] $SpentHours = $CWSpentMin;
    #Reporter has to exist within the system, is not a free form
    [String] $IssueCreator = $CWContactName;
    [String] $Status = "Closed";
    #[String] $description = "Ticket Notes: $Notes"
    #[String] $priority = $CWpriority
    #[String] $priority = 3
    [String] $Board = $CWBoard #customfield_10723
    [String] $DateEntered = $CWDateEntered #customfield_10725
    [String] $DateClosed = $CWDateClosed #customfield_10724
    [String] $EnteredBy = $CWEnteredBy #customfield_10726
    #Scrub company and match to JSD Org ID
    $CCID = $Orgs -match $CWCompanyName -creplace  '.*@ ', ''
    $Description = "Import ConnectWise Issue"
 
    #[String] $body = '{"fields":{"project":{"key":"'+$projectKey+'"},"issuetype":{"name": "'+$issueType+'"},"reporter": {"name": "'+$CWContactName+'"},"labels":["IMPORTED","Connectwise"],"timetracking":{"originalEstimate":"'+$BudgetHours+'","remainingEstimate":"'+$SpentHours+'"},"summary":"'+$summary+'","customfield_10200":"'+$Board+'","customfield_10201":"'+$DateEntered+'","customfield_10202":"'+$CWDateClosed+'","customfield_10203":"'+$EnteredBy+'","customfield_10204":"'+$CWID+'","priority":{"id":"'+$priority+'"}}}';
    [String] $body = '{"fields":{"project":{"key":"'+$projectKey+'"},"assignee":{"name":"'+$Assignee+'"},"issuetype":{"name": "'+$issueType+'"},"reporter": {"name": "'+$CWcontactEmailAddress+'"},"labels":["IMPORTED","Connectwise","'+$CWSTATUS+'"],"summary":"'+$summary+'","description":"'+$description+'","customfield_10108":[{"name":"'+$CWcontactEmailAddress+'"}],"customfield_10110":['+$CCID+'],"priority":{"id":"'+$priority+'"},"customfield_10200":"'+$Board+'","customfield_10201":"'+$DateEntered+'","customfield_10202":"'+$CWDateClosed+'","customfield_10203":"'+$EnteredBy+'","customfield_10500":"'+$CWCompanyName+'","customfield_10204":"'+$CWID+'","customfield_10300":{"id":"'+$urgency+'"},"customfield_10301":{"id":"'+$impact+'"}}}';
    
        ########################
        #Create contacts per org
        #$ContactArray

            #Create Contact

                    [String]$ContactBody = '{"email":"'+$CWcontactEmailAddress+'","fullName":"'+$CWContactName+'"}'
                    $AddContactURI = $target+'/rest/servicedeskapi/customer'
                     try {
                          $JSDheaders = @{
                            "Authorization" = $auth
                            "Content-Type"="application/json"
                            "X-ExperimentalApi"="opt-in"
                             "X-Atlassian-Token"="nocheck"
                             }
                        $AddContact = Invoke-RestMethod -Uri $AddContactURI -Method POST -Headers $JSDheaders -UseBasicParsing -Body $ContactBody
						
                    }
                    catch {
                        Write-Warning "Remote Server Response: $($_.Exception.Message)"
                        Write-Output "Status Code: $($_.Exception.Response.StatusCode)"
                        }
                    #Add Contact or Org after Creating
                        [String]$ContactToOrgBody = '{"usernames": ["'+$CWcontactEmailAddress+'"]}'
                        $AddContactToJSDURI = $target+'/rest/servicedeskapi/organization/'+$CCID+'/user'
                         try {
                              $JSDheaders = @{
                                "Authorization" = $auth
                                "Content-Type"="application/json"
                                "X-ExperimentalApi"="opt-in"
                                 "X-Atlassian-Token"="nocheck"
                                 }
                            $AddContact = Invoke-RestMethod -Uri $AddContactToJSDURI -Method POST -Headers $JSDheaders -UseBasicParsing -Body $ContactToOrgBody
                        }
                        catch {
                            Write-Warning "Remote Server Response: $($_.Exception.Message)"
                            Write-Output "Status Code: $($_.Exception.Response.StatusCode)"
                            }
        #END creation of contacts

    
    function ConvertTo-Base64($string) {
    $bytes = [System.Text.Encoding]::UTF8.GetBytes($string);
    $encoded = [System.Convert]::ToBase64String($bytes);
    return $encoded;
    }
 
    try {
 
    $b64 = ConvertTo-Base64($username + ":" + $password);
    $auth = "Basic " + $b64;
 
    $webRequest = [System.Net.WebRequest]::Create($target+"/rest/api/2/issue/")
    $webRequest.ContentType = "application/json"
    $BodyStr = [System.Text.Encoding]::UTF8.GetBytes($body)
    $webrequest.ContentLength = $BodyStr.Length
    $webRequest.ServicePoint.Expect100Continue = $false
    $webRequest.Headers.Add("Authorization", $auth);
    $webRequest.PreAuthenticate = $true
    $webRequest.Method = "POST"
    $requestStream = $webRequest.GetRequestStream()
    $requestStream.Write($BodyStr, 0, $BodyStr.length)
    $requestStream.Close()
    [System.Net.WebResponse] $resp = $webRequest.GetResponse()
 
    $rs = $resp.GetResponseStream()
    [System.IO.StreamReader] $sr = New-Object System.IO.StreamReader -argumentList $rs
    [string] $results = $sr.ReadToEnd()
    #Write-Output $results
    $ResultINFO = $results | ConvertFrom-JSON
    $JiraIssueID = $ResultINFO.key

    $TotalTNote = $TicketNote.count
    $TotalTAtch = $TicketAttachment.count
    $TotalTTime = $TicketTime.count

            IF(!$TicketNote)
            {
                write-host "No notes detected for ticket: $CWID"
            }
            ELSE
            {
            $N1 = 0
            DO {
                $Notes = "###########DateCreated:"+[string]$TicketNote[$N1].dateCreated + " ContactName:"+[string]$TicketNote[$N1].contact.name + " Notes:"+[string]$TicketNote[$N1].text
                $N1++
                #Convert to JSON and Clean up summary to remove special characters
                $Notes = $Notes -replace '"',"" 
                $Notes = $Notes| ConvertTo-JSON
                $Notes = $Notes -replace '"',"" ` -replace [Environment]::NewLine, "\n"
                    $MaxCharLimit = 32767
                    IF($Notes.Length -gt $MaxCharLimit)
                    {
                    $Notes=$Notes.Substring(0,32700)
                    write-host "Notes were over $MaxCharLimit, trimming to meet character limits"
                    }

                #API call to add comments
                    [String]$Comment = $Notes
                    [String]$ComBody = '{"body":"'+$Comment+'"}'
                    $b64 = ConvertTo-Base64($username + ":" + $password);
                    $auth = "Basic " + $b64;
                    $WRC = [System.Net.WebRequest]::Create($target+"/rest/api/2/issue/$JiraIssueID/comment")
                    $WRC.ContentType = "application/json"
                    $ComStr = [System.Text.Encoding]::UTF8.GetBytes($ComBody)
                    $WRC.ContentLength = $ComStr.Length
                    $WRC.ServicePoint.Expect100Continue = $false
                    $WRC.Headers.Add("Authorization", $auth);
                    $WRC.PreAuthenticate = $true
                    $WRC.Method = "POST"
                    $RSC = $WRC.GetRequestStream()
                    $RSC.Write($ComStr, 0, $ComStr.length)
                    $RSC.Close()
                    [System.Net.WebResponse] $resp = $WRC.GetResponse()
 
                    $CRS = $resp.GetResponseStream()
                    [System.IO.StreamReader] $CSR = New-Object System.IO.StreamReader -argumentList $CRS
                    [string] $CR = $CSR.ReadToEnd()
                    #Write-Output $CR
                    
                    write-host "Comment $N1 of $TotalTNote has been added"
                }
            UNTIL ($N1 -eq $TicketNote.count)
            }

            IF(!$TicketTime)
            {
                write-host "No internal notes detected for ticket: $CWID"
            }
            ELSE
            {
            $T1 = 0
            DO {
                $Times = "###########ID:"+[string]$TicketTime[$T1].id + " Notes:"+[string]$TicketTime[$T1]._info.notes
                $T1++
                #Convert to JSON and Clean up summary to remove special characters
                $Times = $Times -replace '"',"" 
                $Times = $Times| ConvertTo-JSON
                $Times = $Times -replace '"',"" ` -replace [Environment]::NewLine, "\n"
                    $MaxCharLimit = 32767
                    IF($Times.Length -gt $MaxCharLimit)
                    {
                    $Times=$Times.Substring(0,32700)
                    write-host "Notes were over $MaxCharLimit, trimming to meet character limits"
                    }

                #API call to add comments
                    [String]$Comment = $Times
                    [String]$ComBody = '{"body":"'+$Comment+'"}'
                    $b64 = ConvertTo-Base64($username + ":" + $password);
                    $auth = "Basic " + $b64;
                    $WRC = [System.Net.WebRequest]::Create($target+"/rest/api/2/issue/$JiraIssueID/comment")
                    $WRC.ContentType = "application/json"
                    $ComStr = [System.Text.Encoding]::UTF8.GetBytes($ComBody)
                    $WRC.ContentLength = $ComStr.Length
                    $WRC.ServicePoint.Expect100Continue = $false
                    $WRC.Headers.Add("Authorization", $auth);
                    $WRC.PreAuthenticate = $true
                    $WRC.Method = "POST"
                    $RSC = $WRC.GetRequestStream()
                    $RSC.Write($ComStr, 0, $ComStr.length)
                    $RSC.Close()
                    [System.Net.WebResponse] $resp = $WRC.GetResponse()
 
                    $CRS = $resp.GetResponseStream()
                    [System.IO.StreamReader] $CSR = New-Object System.IO.StreamReader -argumentList $CRS
                    [string] $CR = $CSR.ReadToEnd()
                    #Write-Output $CR
                    
                    write-host "Internal Comment (Time Entry) $T1 of $TotalTTime has been added"
                }
            UNTIL ($T1 -eq $TicketTime.count)
            }
        
        #$TotalAttachmentCount
        IF(!$TicketAttachment)
        {
            write-host "No attachments detected for ticket: $CWID"
            }
        ELSE
            {
        $A1 = 0
            DO {

                $ContentType = "application/form-data"
                $Attachment = $TicketAttachment[$A1]
                #$Attachment
                #Gather attachment information, scrub file and restore to be uploaded later.
                $AID = $Attachment.id
                $FileName = $Attachment._info.filename
                $DocumentDownHREF = $Attachment._info.documentDownload_href
                $document_href = $Attachment._info.document_href 
                $FileName = $FileName -replace "[^a-zA-Z0-9_.]", ""
                $OutPutPath = "C:\ContractWork\Appno\Attachments\$AID-$FileName"
                [string]$BaseAttchDLUri = "$CWServerRoot" + "v4_6_release/apis/3.0/system/documents/$AID/download"
                Invoke-RestMethod -Headers $Header -Uri $BaseAttchDLUri -ContentType $ContentType -Method GET -Outfile $OutPutPath

                $FilePath = "C:\ContractWork\Appno\Attachments\$AID-$FileName"
                $attachfile = @{body=@{File="$FilePath"}}
                $ATJson = ConvertTo-Json $attachfile
                #COMMENT THIS OUT FOR TESTING, UNCOMMENT FOR PRODUCTION!
                $JIRAIssueURL = "$JIRAURL/rest/api/2/issue/$JiraIssueID/attachments"

                    #Write out attachments to CURL script to be executed and uploaded later.
                    #FOR TESTING:
                    #UNCOMMENT THIS FOR TESTING, COMMENT OUT FOR PRODUCTION!
                    #$JIRAIssueURL = "$JIRAURL/rest/api/2/issue/TEST-79592/attachments"
                    $ACMD = '-u '+$username+':'+$password+' -X POST -H "X-Atlassian-Token: nocheck" -F "file=@'+$FilePath+'" '+$JIRAIssueURL
                    #$CURL + $ACMD
                    Start-Process  $CURL $ACMD
                    #$AttachmentCMD = '"C:\Program Files\curl\bin\curl.exe" -v -S -u '+$username+':'+$password+' -X POST -H "X-Atlassian-Token: nocheck" -F "file=@'+$FilePath+'" '+$JIRAIssueURL
                    #$AttachmentCurl | Out-File -FilePath "C:\ContractWork\Appno\CurlAttachmentCall.txt" -append
                $A1++
                write-host "Attachment $A1 of $TotalTAtch has been added"
                }
            UNTIL ($A1 -eq $TicketAttachment.count)
                }
 
            }
 
    catch [System.Net.WebException]{
            if ($_.Exception -ne $null -and $_.Exception.Response -ne $null) {
                $errorResult = $_.Exception.Response.GetResponseStream()
                $errorText = (New-Object System.IO.StreamReader($errorResult)).ReadToEnd()
                Write-Warning "The remote server response: $errorText"
                Write-Output $_.Exception.Response.StatusCode
                    [string]$i1+"|"+[string]$CWID+"|"+$errorText  | out-file "C:\ContractWork\Appno\debug.log" -Append

            } else {
                throw $_
            }
        }

    IF($debug -gt 0)
    {
    write-host "Processing tickets, just finished CW Ticket: $CWID"
    write-host "#################"
    }
    ELSE
    {
    echo Debug disabled
    }
   
    write-host "Processed $i1 of $C1"
    $i1++
    }
    #FOR DEBUGGING, COMMENT OUT FOR ACTUAL USE
    UNTIL ($i1 -eq 27050)
    #UNTIL ($i1 -eq $C1)
    write-host "Jira issues created"